package samapi2::SessionType;

use constant {
	STREAM => "STREAM",
	DATAGRAM => "DATAGRAM",
	RAW => "RAW",
	PRIMARY => "PRIMARY",
	MASTER => "MASTER"
};

package samapi2::SignatureType;

use constant {
	DSA_SHA1 => "DSA_SHA1",
	ECDSA_SHA256_P256 => "ECDSA_SHA256_P256",
	ECDSA_SHA384_P384 => "ECDSA_SHA384_P384",
	ECDSA_SHA512_P521 => "ECDSA_SHA512_P521",
	RSA_SHA256_2048 => "RSA_SHA256_2048",
	RSA_SHA384_3072 => "RSA_SHA384_3072",
	RSA_SHA512_4096 => "RSA_SHA512_4096",
	EdDSA_SHA512_Ed25519 => "EdDSA_SHA512_Ed25519",
	EdDSA_SHA512_Ed25519ph => "EdDSA_SHA512_Ed25519ph",
	RedDSA_SHA512_Ed25519 => "RedDSA_SHA512_Ed25519"
};

package samapi2;

use samapi;
use sammsg;

use parent "samapi";

sub new {
	
	my $class = $_[0];
	my $self = {
		
	};
	
	my $samapi = $class->samapi::new($_[1], $_[2]);
	$self = {%$self, %$samapi};
	
	return bless($self, $class);
	
}

sub lookup {
	my $self = $_[0];
	
	my $ans = $self->sendcommand(sammsg->create("naming", "lookup", {
		"name" => $_[1]
	}));
	
	$ans->getArg("result") eq "OK" || die $ans->getArg("result") . " - " . $ans->getArg("message");
	
	return $ans->getArg("value");
}

sub ping {
	my $self = $_[0];
	my $sock = $self->{sock};
	
	if(defined $_[1]) {
		print $sock "PING $_[1]\n";
		my $ansstr = <$sock>;
		chomp $ansstr;
		return true if $ansstr eq "PONG $_[1]";
	} else {
		print $sock "PING\n";
		my $ansstr = <$sock>;
		chomp $ansstr;
		return true if $ansstr eq "PONG";
	}
	
	return false;
	
}

sub help {
	my $self = $_[0];
	my $sock = $self->{sock};
	
	if(defined $_[1]) {
		print $sock "HELP $_[1]\n";
	} else {
		print $sock "HELP\n";
	}
	
	my $ansstr = <$sock>;
	chomp $ansstr;
	return $ansstr;
}

1;