package samapi;

use IO::Socket;
use sammsg;

sub new {
	
	my $class = $_[0];
	my $self = {
		host => "127.0.0.1",
		port => 7656,
		username => "",
		password => "",
		sock => undef,
		samversion => undef
	};
	
	if(defined $_[1]) {
		$self->{host} = $_[1];
	}
	
	if(defined $_[2]) {
		$self->{port} = $_[2];
	}
	
	return bless($self, $class);
	
}

sub connect {
	
	my $self = $_[0];
	
	$self->{sock} = IO::Socket->new(
		PeerAddr => $self->{host},
		PeerPort => $self->{port},
		Domain => IO::Socket::AF_INET,
		Type => IO::Socket::SOCK_STREAM,
		Proto => "tcp"
	);
	my $sock = $self->{sock};
	$sock->connected() || die "Can not open socket - $!";
	
	if(defined $_[1]) {
		$self->{username} = $_[1];
	}
	
	if(defined $_[2]) {
		$self->{password} = $_[2];
	}
	
	print $sock sammsg->create("HELLO", "VERSION", {
		user => $self->{username},
		password => $self->{password}
	})->getMessage();
	my $ansstr = <$sock>;
	my $msg = sammsg->parse($ansstr);
	if($msg->getArg("result") ne "OK") {
		warn "HELLO VERSION RESULT != OK - is *" . $msg->getArg("result") . "*";
		return 0;
	}

	$self->{samversion} = $msg->getArg("version");
	
	return 1;
}

sub version {
	my $self = $_[0];
	return $self->{samversion};
}

sub sendrowcommand {
	my $self = $_[0];
	my $sock = $self->{sock};

	print $sock $_[1];
	return <$sock>;
}

sub sendcommand {
	my $self = $_[0];
	my $sock = $self->{sock};

	print $sock $_[1]->getMessage();
	my $ansstr = <$sock>;
	return sammsg->parse($ansstr);
}

sub close {
	my $self = $_[0];
	my $sock = $self->{sock};
	
	if(! $sock->connected()) {
		print $sock "QUIT\n";
	
		$self->{sock}->close();
	}
}

sub isClosed {
	my $self = $_[0];
	
	return $self->{isClosed};
}

sub DESTROY {
	my $self = $_[0];
	my $sock = $self->{sock};
	
	if(! $sock->connected()) {
		$self->close();
	}
}

1;