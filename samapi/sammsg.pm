package sammsg;

use strict;
use warnings;

sub create {
	
	my $class = $_[0];
	my $self = {
		message => undef,
		args => $_[3],
		command => $_[1],
		subcmd => $_[2]
	};
	
	$self->{message} = uc($self->{command}) . " " . uc($self->{subcmd});
	
	foreach my $key (keys %{$self->{args}}) {
		if($self->{args}->{$key} ne "") {
			if($self->{args}->{$key} =~ /[ ~-]/) {
				$self->{message} = "$self->{message} " . uc($key) . "=\"$self->{args}->{$key}\"";
			} else {
				$self->{message} = "$self->{message} " . uc($key) . "=$self->{args}->{$key}";
			}
		}
	}
	
	$self->{message} = "$self->{message}\n";
	
	return bless($self, $class);
	
}

sub parse {
	
	my $class = $_[0];
	my $self = {
		message => $_[1],
		args => {},
		command => undef,
		subcmd => undef
	};
	
	my $firpos = index $_[1], " ";
	my $secpos = index($_[1], " ", $firpos + 1);
	$self->{command} = substr($_[1], 0, $firpos);
	$self->{subcmd} = substr($_[1], $firpos + 1, $secpos - $firpos - 1);
	
	my $endpos = $secpos;
	my $eqsign;
	my $key;
	my $val;
	my $sumlenmo = length($_[1]) - 1;
	
	while($endpos != $sumlenmo) {
		$eqsign = index($_[1], "=", $endpos + 1);
		$key = substr($_[1], $endpos + 1, $eqsign - $endpos - 1);
		#print "Key: $key\n";
		if(substr($_[1], $eqsign + 1, 1) eq "\"") {
			$endpos = index($_[1], "\"", $eqsign + 2);
			$endpos = $sumlenmo if $endpos == -1;
			$val = substr($_[1], $eqsign + 2, $endpos - $eqsign - 2);
		} else {
			$endpos = index($_[1], " ", $eqsign);
			$endpos = $sumlenmo if $endpos == -1;
			$val = substr($_[1], $eqsign + 1, $endpos - $eqsign - 1);
		}
		#print "Value: $val\n";
		$self->{args}->{$key} = $val;
	}
	
	return bless($self, $class);
	
}

sub getMessage {
	my $self = $_[0];
	return $self->{message};
}

sub getPrimCommand {
	my $self = $_[0];
	return $self->{command};
}

sub getSecoCommand {
	my $self = $_[0];
	return $self->{subcmd};
}

sub getArgs {
	my $self = $_[0];
	return $self->{args};
}

sub getArg {
	my $self = $_[0];
	if(exists $self->{args}->{uc $_[1]}) {
		return $self->{args}->{uc $_[1]};
	} elsif (exists $self->{args}->{$_[1]}) {
		return $self->{args}->{$_[1]};
	}
	return undef;
}

1;